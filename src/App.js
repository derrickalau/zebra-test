import React from 'react';
import './App.css';
import "./cartSection";

function App() {
  return (
    <div className="mobile-background-light-standa">
      <header className="mobile-header-standard">
        <h1 className="page-title">Catalog</h1>
      </header>
        <h2 className="pandora">Pandora</h2>
        <h3 className="secondary-information">Enjoys long walks at sunset</h3>
        <cartSection catImage="" />
        <h2 className="pandora">Fluffykins</h2>
        <h3 className="secondary-information">Enjoys mice </h3>
        <cartSection catImage=""/>
        <h2 className="pandora">Dave</h2>
        <h3 className="secondary-information">Enjoys sleeping</h3>
        <cartSection catImage=""/>
    </div>
  );
}

export default App;
